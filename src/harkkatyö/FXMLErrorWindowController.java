package harkkatyö;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/** 
 * Author: Teemu Tynkkynen
 * Student number: 0418815
 * Date: 16.12.2015
 * 
 * FXML controller for error window
 */

public class FXMLErrorWindowController implements Initializable {
    @FXML
    private Label errorLabel;
    Main main = Main.getInstance();
    @FXML
    private Button close;

    /**
     * Initializes the controller class.
     * 
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        errorLabel.setText(main.getError());
    }    
    /**
     * Closes window
     * 
     * @param event 
     */
    @FXML
    private void handleCloseAction(ActionEvent event) {
        Stage stage = (Stage)close.getScene().getWindow();
        stage.close();
    }   
}
