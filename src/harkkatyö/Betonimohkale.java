package harkkatyö;

/**
 * Author: Teemu Tynkkynen
 * Student number: 0418815
 * Date: 17.12.2015
 */
public class Betonimohkale extends ThirdClass{
    
    public Betonimohkale(){
        super("Betonimöhkäle", "200x200x200", 200.00, false);
    } 
}
