package harkkatyö;

import java.util.ArrayList;

/**
 * Author: Teemu Tynkkynen
 * Student number: 0418815
 * Date: 16.12.2015
 *
 * Package class
 */
public class Package {
    protected String name;
    protected String size;
    protected double weight;
    protected boolean breakable; // True if item is breakable
    protected double distance;
    protected int id; // ID of package
    protected int clas; // Class of package
    protected String to; // Target city
    protected String from; // Starting city
    protected ArrayList<String> list = new ArrayList<>();
    
    public Package(String na, String si, double we, boolean b, int cl){
        name = na;
        size = si;
        weight = we;
        breakable = b;
        clas = cl; 
    }
    
    /**
     * Gives name of package
     * 
     * @return 
     */
    public String getName(){
        return name;
    }
    
    /**
     * Gives size of package
     * 
     * @return 
     */
    public String getSize(){
        return size;
    }
    
    /**
     * Gives weight of package
     * 
     * @return 
     */
    public double getWeight() {
        return weight;
    }
    
    /**
     * Sets array of package
     * 
     * @param array 
     */
    public void setArray(ArrayList<String> array){
        list = array;
    }
    
    /**
     * Sets starting city
     * 
     * @param a 
     */
    public void setFrom(String a){
        from = a;
    }
    
    /**
     * Sets target city
     * 
     * @param a 
     */
    public void setTo(String a){
        to = a;
    }
    
    /**
     * Gives starting city
     * 
     * @return 
     */
    public String getFrom(){
        return from;
    }
    
    /**
     * Gives target city
     * 
     * @return 
     */
    public String getTo(){
        return to;
    }
    
    /**
     * Sets distance
     * 
     * @param i 
     */
    public void setDistance(double i){
        distance = i;
    }
    
    /**
     * Gives distance
     * 
     * @return 
     */
    public double getDistance(){
        return distance;
    }
        
    /**
     * Gives array
     * 
     * @return 
     */
   public ArrayList<String> getArray(){
       return list;
   }
   
   /**
    * Sets ID of package
    * 
    * @param i 
    */
   public void setId(int i){
        id = i;
    }
    
   /**
    * Gives ID of package
    * 
    * @return 
    */
    public int getId(){
        return id;
    }
    
    /**
     * Gives class of package
     * 
     * @return 
     */
    public int getClas(){
        return clas;
    }
    
    /**
     * Sets class of package
     * 
     * @param i 
     */
    public void setClas(int i){
        clas = i;
    }
    
    /**
     * Gives breakablity of package
     * 
     * @return 
     */
    public boolean getBreakable(){
        return breakable;
    }
}
