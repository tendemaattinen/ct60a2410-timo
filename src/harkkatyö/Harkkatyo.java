package harkkatyö;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;


/**
 * Author: Teemu Tynkkynen
 * Student number: 0418815
 * Date: 16.12.2015
 *
 * Class which includes main method and starts application
 */
public class Harkkatyo extends Application {
    
    Main main = Main.getInstance();
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("oiminnaltaan Itellaa Muistuttava Ohjelmisto");
        stage.getIcons().add(new Image(getClass().getResourceAsStream("res/T-logo.png")));
        stage.show();
        // Makes logs when application closes
        main.checkLogAndStorage();
        stage.setOnCloseRequest((WindowEvent we) -> {
            main.addToLog(main.getTime() + " Ohjelma suljettu, " + main.getSize() + " pakettia varastoitu");
            try {
                main.makeLogFile();
                main.makeStorageLog();
            } catch (IOException ex) {
                Logger.getLogger(Harkkatyo.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    } 
}
