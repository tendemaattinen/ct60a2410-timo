package harkkatyö;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/** 
 * Author: Teemu Tynkkynen
 * Student number: 0418815
 * Date: 15.12.2015
 * 
 * FXML controller for Ask Log window
 */
public class FXMLAskLogController implements Initializable {
    
    @FXML
    private Button yesButton;
    @FXML
    private Button noButton;
    
    Main main = Main.getInstance();

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {  
    }    
    
    /**
     * Restore storage and log
     * 
     * @param event
     * @throws IOException 
     */
    @FXML
    private void handleYesButtonAction(ActionEvent event) throws IOException {
        main.readLog();
        main.readStorageLog();
        Stage stage = (Stage)noButton.getScene().getWindow();
        stage.close();
    }
    
    /**
     * Continue without restoring storage and log
     * 
     * @param event 
     */
    @FXML
    private void handleNoButtonAction(ActionEvent event) {
        main.addToLog(main.getTime() + " Ohjelma kännistetty");
        Stage stage = (Stage)noButton.getScene().getWindow();
        stage.close();
    }
}
