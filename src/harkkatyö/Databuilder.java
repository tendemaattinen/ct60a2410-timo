package harkkatyö;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/** 
 * Author: Teemu Tynkkynen
 * Student number: 0418815
 * Date: 15.12.2015
 * 
 * Databuilding class, reads data from XML, parsing it and saves data to arraylist
 */

public class Databuilder {
    
    private Document doc;
    private ArrayList<SmartPost> SmartP = new ArrayList<>();
    private int lenght = 0;
    private SmartPost smartPo;
    
    
    public Databuilder(){
        
    }
    
    /** 
     * Read data from website and returns it
     * 
     * @param url
     * @return content
     * @throws java.io.IOException
     * 
     */
    public String content(URL url) throws IOException{
        String content;
        BufferedReader br;
        br = new BufferedReader(new InputStreamReader(url.openStream()));
        content = "";
        String line;
        while ((line = br.readLine()) != null) {
            content += line + "\n";
        }
        return content;
    }
    
    /** 
     * Builder
     * 
     * @param content
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException 
     */
    public void builder(String content) throws ParserConfigurationException, SAXException, IOException{
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        doc = dBuilder.parse(new InputSource(new StringReader(content)));
        parseSmartPost();
        
        
    }
    
    /**
     * Parse data and save it to arraylist
     */
    private void parseSmartPost(){
        NodeList nodes = doc.getElementsByTagName("place");
        for (int i = 0; i < nodes.getLength(); i++){
            Node node = nodes.item(i);
            Element e = (Element) node;
            smartPo = new SmartPost(getValue("code", e), getValue("city", e), getValue("address", e), getValue("availability", e), getValue("postoffice", e), getValue("lat", e), getValue("lng", e));
            SmartP.add(smartPo);
            lenght = lenght + 1;
        }
    }
    
    /**
     * @param tag
     * @param e
     * @return 
     */
    private String getValue(String tag, Element e) {
        return ((Element)e.getElementsByTagName(tag).item(0)).getTextContent();
    }
    
    /** Gives number of smartposts
     * 
     * @return 
     */
    public int getLenght() {
        return lenght;
    }
    
    /** 
     * Gives name of post office from arraylist
     * 
     * @param i
     * @return 
     */
    public String getPostOffice(int i){
        return SmartP.get(i).getPostOffice();
    }
    
    /**
     * Gives name of city from arraylist
     * 
     * @param i
     * @return 
     */
    public String getCity(int i){
        return SmartP.get(i).getCity();
    }
    
    /** 
     * Gives latiude of given postoffice
     * 
     * @param s
     * @return 
     */
    public String getLat(String s){
        String c = "";
        for (int i = 0; i < lenght; i++) {
            if (s.equals(SmartP.get(i).getPostOffice())){
                c =  SmartP.get(i).getLat();
            }
        }
        return c;
    }
    
    /** 
     * Gives longitude of given postoffice
     * 
     * @param s
     * @return 
     */
    public String getLng(String s){
        String c = "";
        for (int i = 0; i < lenght; i++) {
            if (s.equals(SmartP.get(i).getPostOffice())){
                c =  SmartP.get(i).getLng();
            }
        }
        return c;
    }
    
    /** 
     * Gives addres of postoffice from arraylist
     * 
     * @param i
     * @return 
     */
    public String getAddress(int i){
        return SmartP.get(i).getAddress();
    }
    
    /** 
     * Gives postcode of smartpost
     * 
     * @param i
     * @return 
     */
    public String getCode(int i){
        return SmartP.get(i).getCode();
    }
    
    /** 
     * Gives avaibility of smartpost
     * 
     * @param i
     * @return 
     */
    public String getAvailability(int i){
        return SmartP.get(i).getAvailability();
    }
    
}
