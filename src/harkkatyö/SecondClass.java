package harkkatyö;

/**
 * Author: Teemu Tynkkynen
 * Student number: 0418815
 * Date: 17.12.2015
 *
 * Second class package
 */
public class SecondClass  extends Package{
    
    public SecondClass(String a, String b, double c, boolean d){
        super(a,b,c,d,2);
    } 
}
