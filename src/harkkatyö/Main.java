package harkkatyö;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.web.WebEngine;
import javafx.stage.Stage;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/** Author: Teemu Tynkkynen
 *  Student number: 0418815
 *  Date: 17.12.2015
 * 
 * Class which connects storage, smartpost and windows
 */

public class Main {
    
    private Databuilder data = new Databuilder();
    private static final Main instance = new Main();
    private Storage sto = Storage.getInstance();
    private String errorString;
    private WebEngine engine = new WebEngine();
    private Log log = new Log();
    
    private Main() {
        // Loads index.html and calls Databuilder class where it parses data from given URL
        engine.load(getClass().getResource("res/index.html").toExternalForm());
        String urli = "http://smartpost.ee/fi_apt.xml";
        try {
            URL url = new URL(urli);
            String a;
            try {
                a = data.content(url);
                try {
                    data.builder(a);
                } catch (ParserConfigurationException | SAXException | IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static Main getInstance(){
        return instance;
    }
    
    /** 
     * Gives number of smartposts
     * 
     * @return 
     */
    public int getLenght(){
        int x = data.getLenght();
        return x;
    }
    
    /** Gives name of city in arrays location x
     * 
     * @param x
     * @return 
     */
    public String getCity(int x) {
        String a = data.getCity(x);
        return a;
    }
    
    /** 
     * Gives latitude of given smartpost
     * 
     * @param a
     * @return 
     */
    public String getLat(String a){
        return data.getLat(a);
    }
    
    /** 
     * Gives longitude of given smartpost
     * 
     * @param a
     * @return 
     */
    public String getLng(String a){
        return data.getLng(a);
    }
    
    /** 
     * Gives name of postoffice
     * 
     * @param x
     * @return 
     */
    public String getPostOffice(int x) {
        String a = data.getPostOffice(x);
        return a;
    }
    
    /** 
     * Makes string for marker
     * 
     * @param x
     * @return 
     */
    public String addToMap(int x) {
        String xxx = ("'" + data.getAddress(x) + ", " + data.getCode(x) + " " + data.getCity(x)
                      + "', '" + data.getPostOffice(x).substring(19) + ", " + data.getAvailability(x) + "'");
        return xxx;
    }
    
    /** 
     * Gives name of package in storage
     * 
     * @param i
     * @return 
     */
    public Package getPackage(int i){
        return sto.getPackage(i);
    }
    
    /**
     * Gives size of storage
     * 
     * @return 
     */
    public int getSize(){
        return sto.getSize();
    }
    
    /** 
     * Gives id of package in storage
     * 
     * @param i
     * @return 
     */
    public int getId(int i){
        return sto.getId(i);
    }
    
    /** 
     * Gives name of predefined item
     * 
     * @param i
     * @return 
     */
    public String getNameFromList2(int i){
        return sto.getNameFromList2(i);
    }
    
    /** Handles item to storage class
     * 
     * @param name
     * @param list
     * @param clas
     * @param from
     * @param to 
     */
    public void handleItem(String name, ArrayList<String> list, int clas, String from, String to){
        String distance = engine.executeScript("document.createPath("+ list + ", 'red', '1')").toString();
        double dist = Double.parseDouble(distance);
        sto.createPackageFromItem(name, list, dist, clas, from , to);
    }
    
    /** 
     * Gives name of item in storage
     * 
     * @param i
     * @return 
     */
    public String getName(int i){
        return sto.getName(i);
    }
    
    /** 
     * Sets error code which FXMLErrorController retrieve
     * 
     * @param errorText 
     */	
    public void setError(String errorText){
        errorString = errorText;
        
    }
    
    /** 
     * Gives error message to error window
     * 
     * @return 
     */
    public String getError(){
        return errorString;
    }
    
    /**
     * Creates error window
     * 
     * @throws IOException 
     */
    public void createErrorWindow() throws IOException{
        Stage stage = new Stage();
        Parent page = FXMLLoader.load(getClass().getResource("FXMLErrorWindow.fxml"));
        Scene scene = new Scene(page);
        stage.setScene(scene);
        stage.setTitle("Virheilmoitus");
        stage.getIcons().add(new Image(getClass().getResourceAsStream("res/T-logo.png")));
        stage.show();
    }
    
    /** 
     * Gives arraylist which includes starting and target lat and lng
     * 
     * @param i
     * @return 
     */
    public ArrayList<String> getArray(int i){
        return sto.getArray(i);
    }
    
    /**
     * Remove package from storage
     * 
     * @param i 
     */
    public void removePackage(int i){
        sto.remove(i);
    }
    
    /** 
     * Checks if given string are numbers
     * 
     * @param a
     * @param b
     * @return true if all chars are number, otherwise return false
     * @throws IOException 
     */
    public boolean checkNumber(String[] a, int b) throws IOException{
        char x = '.';
        for (String a1 : a) {
            for (int j = 0; j < a1.length(); j++) {
                char c = a1.charAt(j);
                if ((Character.isDigit(c)) || (c == x)){
                        
                } else {
                    if (b == 0){
                        setError("Koko-kenttä virheellinen");
                        createErrorWindow();
                        return false;
                    } else {
                        setError("Paino-kenttä virheellinen");
                        createErrorWindow();
                        return false;
                    }
                }
            }  
        }
        return true;   
    }
    
    /** 
     * Makes arraylist from starting and target lat and lng and returns it
     * 
     * @param slat
     * @param slng
     * @param tlat
     * @param tlng
     * @return 
     */
    public ArrayList<String> makeArray(String slat, String slng, String tlat, String tlng){
        ArrayList<String> list = new ArrayList<>();
        list.add(slat); 
        list.add(slng); 
        list.add(tlat); 
        list.add(tlng);
        return list;
    }
    
    /** 
     * Gives distance between two automatons
     * 
     * @param list
     * @return 
     */
    public double makeDistance(ArrayList<String> list){
        String distance = engine.executeScript("document.createPath("+ list + ", 'red', '1')").toString();
        double dist = Double.parseDouble(distance);
        return dist;
    }
    
    /** 
     * Gives class of package in storage
     * 
     * @param i
     * @return 
     */
    public int getClas(int i){
        return sto.getClas(i);
    }
    
    /** 
     * Creates new package from information given by user
     * 
     * @param name
     * @param size
     * @param weight
     * @param bre
     * @param cla
     * @param array
     * @param dist
     * @param from
     * @param to 
     */
    public void createPackage(String name, String size, double weight, boolean bre, int cla, ArrayList<String> array, double dist, String from, String to){
        sto.createNewPackage(name, size, weight, bre, cla, array, dist, from, to);
    }
    
    /** 
     * Gives size of storage
     * 
     * @return 
     */
    public int getLenght2(){
        return sto.getSize();
    }
    
    /** 
     * Opens log window
     * 
     * @throws IOException 
     */
    public void openLog() throws IOException{
        Stage stage = new Stage();
        Parent page = FXMLLoader.load(getClass().getResource("FXMLLogWindow.fxml"));
        Scene scene = new Scene(page);
        stage.setScene(scene);
        stage.getIcons().add(new Image(getClass().getResourceAsStream("res/T-logo.png")));
        stage.setTitle("Loki");
        stage.show();
    }
    
    /** 
     * Gives distance from package
     * 
     * @param i
     * @return 
     */
    public double getDistance(int i){
        return sto.getDistance(i);
    }
    
    /** 
     * Adds text to log
     * 
     * @param a 
     */
    public void addToLog(String a){
        log.addToLog(a + "\n");
    }
    
    /** 
     * Gets text from log
     * 
     * @param i
     * @return 
     */
    public String getFromLog(int i){
        return log.getFromLogArray(i);
    }
    
    /** 
     * Gives lenght of log
     * 
     * @return 
     */
    public int getLogLenght(){
        return log.getArrayLenght();
    }
    
    /** 
     * Gives current time
     * 
     * @return 
     */
    public String getTime(){
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        Date date = new Date();
        String time = df.format(date);
        return time;
    }
    
    /** 
     * Gives true if item is breakable, if it isn't gives false
     * 
     * @param i
     * @return 
     */
    public boolean getB(int i) {
        return sto.getBreakable(i);
    }
    
    /** Make logfile
     * 
     * @throws IOException 
     */
    public void makeLogFile() throws IOException{
        log.makeLogFile();
    }
    
    /** Reads log file
     * 
     * @throws IOException 
     */
    public void readLog() throws IOException{
        log.readLog(getTime());
    }
    
    /** 
     * Check if there are log.txt and storageLog.txt files
     * 
     * @throws IOException 
     */
    public void checkLogAndStorage() throws IOException{
        File f1 = new File("log.txt");
        File f2 = new File("storageLog.txt");
        // If files exists, opens window where it asks if user wants load old log and storage
        if ((f1.exists()) && (f2.exists())){
            Stage stage = new Stage();
            Parent page = FXMLLoader.load(getClass().getResource("FXMLAskLog.fxml"));
            Scene scene = new Scene(page);
            stage.setScene(scene);
            stage.setTitle("Loki ja Varasto");
            stage.show();
        } else {
            addToLog(getTime() + " Ohjelma käynnistetty");
        }
    }
    
    /** 
     * Makes storagelog
     * 
     * @throws IOException 
     */
    public void makeStorageLog() throws IOException{
            sto.makeStorageLog();   
    }
    
    /** 
     * Reads storagelog
     * 
     * @throws IOException 
     */
    public void readStorageLog() throws IOException{
        sto.readStorageLog();
    } 
    
    /** 
     * Gives starting location of package
     * 
     * @param i
     * @return 
     */
    public String getFrom(int i){
        return sto.getFrom(i);
    }
    
    /** 
     * Gives target location of package
     * 
     * @param i
     * @return 
     */
    public String getTo(int i){
        return sto.getTo(i);
    }
}
