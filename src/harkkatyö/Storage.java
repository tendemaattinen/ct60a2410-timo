package harkkatyö;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Author: Teemu Tynkkynen
 * Student number: 0418815
 * Date: 16.12.2015
 *
 * Storage class
 */
public class Storage {
    
    private static final Storage instance = new Storage();
    private ArrayList<Package> list = new ArrayList<>(); // List of packages in storage
    private ArrayList<String> list2 = new ArrayList<>(); // List of predefined items
    private int size = 0; // Size of storage
    private int id = 0;
    
    private Storage(){
        Package q = new Kahvipaketti();
        Package w = new Pitsa();
        Package e = new Kalle();
        Package r = new Tietokone();
        Package t = new Betonimohkale();
        Package y = new Renkaat();
        list2.add(q.getName());
        list2.add(w.getName());
        list2.add(e.getName());
        list2.add(r.getName());
        list2.add(t.getName());
        list2.add(y.getName());
    }
    
    public static Storage getInstance(){
        return instance;
    }
    
    /**
     * Gives package from storage
     * 
     * @param i
     * @return 
     */
    public Package getPackage(int i){
        return list.get(i);
    }
    
    /**
     * Gives size of storage
     * 
     * @return 
     */
    public int getSize(){
        return size;
    }
    
    /**
     * Gives predefined name from list
     * 
     * @param i
     * @return 
     */
    public String getNameFromList2(int i){
        return list2.get(i);
    }
    
    /**
     * Create package from predefined item
     * 
     * @param a
     * @param array
     * @param dist
     * @param x
     * @param from
     * @param to 
     */
    public void createPackageFromItem(String a, ArrayList<String> array, double dist, int x, String from, String to){
        for (int i = 0; i < list2.size();i++){
            String b = list2.get(i);
            if (a.equals(b)) {
                if (i == 0){
                    id += 1;
                    Package pack = new Kahvipaketti();
                    pack.setArray(array);
                    pack.setDistance(dist);
                    pack.setId(id);
                    pack.setClas(x);
                    pack.setFrom(from);
                    pack.setTo(to);
                    list.add(pack);
                    size += 1;
                }
            }
            if (a.equals(b)) {
                if (i == 1){
                    id += 1;
                    Package pack = new Pitsa();
                    pack.setArray(array);
                    pack.setDistance(dist);
                    pack.setId(id);
                    pack.setClas(x);
                    pack.setFrom(from);
                    pack.setTo(to);
                    list.add(pack);
                    size += 1;
                }
            }
            if (a.equals(b)) {
                if (i == 2){
                    id += 1;
                    Package pack = new Kalle();
                    pack.setArray(array);
                    pack.setDistance(dist);
                    pack.setId(id);
                    pack.setClas(x);
                    pack.setFrom(from);
                    pack.setTo(to);
                    list.add(pack);
                    size += 1;
                }
            }
            if (a.equals(b)) {
                if (i == 3){
                    id += 1;
                    Package pack = new Tietokone();
                    pack.setArray(array);
                    pack.setDistance(dist);
                    pack.setId(id);
                    pack.setClas(x);
                    pack.setFrom(from);
                    pack.setTo(to);
                    list.add(pack);
                    size += 1;
                }
            }
            if (a.equals(b)) {
                if (i == 4){
                    id += 1;
                    Package pack = new Betonimohkale();
                    pack.setArray(array);
                    pack.setDistance(dist);
                    pack.setId(id);
                    pack.setClas(x);
                    pack.setFrom(from);
                    pack.setTo(to);
                    list.add(pack);
                    size += 1;
                }
            }
            if (a.equals(b)) {
                if (i == 5){
                    id += 1;
                    Package pack = new Renkaat();
                    pack.setArray(array);
                    pack.setDistance(dist);
                    pack.setId(id);
                    pack.setClas(x);
                    pack.setFrom(from);
                    pack.setTo(to);
                    list.add(pack);
                    size += 1;
                }
            }
        }
    }
    
    /**
     * Create package from information given by user
     * 
     * @param name
     * @param sizet
     * @param weight
     * @param bre
     * @param cla
     * @param array
     * @param dist
     * @param from
     * @param to 
     */
    public void createNewPackage(String name, String sizet, double weight, boolean bre, int cla, ArrayList<String> array, double dist, String from, String to){
        if (cla == 1){
            Package pack = new FirstClass(name, sizet, weight, bre);
            id += 1;
            pack.setArray(array);
            pack.setDistance(dist);
            pack.setId(id);
            pack.setFrom(from);
            pack.setTo(to);
            list.add(pack);
            size += 1;
        }
        else  if (cla == 2){
            Package pack = new SecondClass(name, sizet, weight, bre);
            id += 1;
            pack.setArray(array);
            pack.setDistance(dist);
            pack.setId(id);
            pack.setFrom(from);
            pack.setTo(to);
            list.add(pack);
            size += 1;
        }
        else {
            Package pack = new ThirdClass(name, sizet, weight, bre);
            id += 1;
            pack.setArray(array);
            pack.setDistance(dist);
            pack.setId(id);
            pack.setFrom(from);
            pack.setTo(to);
            list.add(pack);
            size += 1; 
        }
    }
    
    /**
     * Gives name from package
     * 
     * @param i
     * @return 
     */
    public String getName(int i){
        return list.get(i).getName();           
    }
    
    /**
     * Gives array from package
     * 
     * @param i
     * @return 
     */
    public ArrayList<String> getArray(int i){
        return list.get(i).getArray();
    }
    
    /**
     * Gives distance from package
     * 
     * @param i
     * @return 
     */
    public double getDistance(int i){
        return list.get(i).getDistance();
    }
    
    /**
     * Gives ID of package
     * 
     * @param i
     * @return 
     */
    public int getId(int i){
        return list.get(i).getId();
    }
    
    /**
     * Remove package from storage
     * 
     * @param i 
     */
    public void remove(int i){
        list.remove(i);
        size -= 1;
    }
    
    /**
     * Gives class of package
     * 
     * @param i
     * @return 
     */
    public int getClas(int i){
        return list.get(i).getClas();
    }
    
    /**
     * Gives breakablity of package
     * 
     * @param i
     * @return 
     */
    public boolean getBreakable(int i) {
        return list.get(i).getBreakable();
    }
    
    /**
     * Makes storage log
     * 
     * @throws IOException 
     */
    public void makeStorageLog() throws IOException{
        try (BufferedWriter out = new BufferedWriter(new FileWriter("storageLog.txt"))) {
            for (int i = 0; i < size; i++){
                out.write(list.get(i).getName() + ";" + list.get(i).getSize() + ";" + list.get(i).weight + ";" +
                        list.get(i).getBreakable() + ";" + list.get(i).getDistance() + ";" + list.get(i).getId() + ";" +
                        list.get(i).getClas() + ";" + list.get(i).getArray().get(0) + ";" + list.get(i).getArray().get(1) +
                        ";" +list.get(i).getArray().get(2) + ";" + list.get(i).getArray().get(3) + ";" + list.get(i).getFrom() + ";" + list.get(i).getTo());
                out.newLine();
            }
        }
    }
    
    /**
     * Reads storage log and add packages back to storage
     * 
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public void readStorageLog() throws FileNotFoundException, IOException{
        try (BufferedReader in = new BufferedReader (new FileReader("storageLog.txt"))) {
            String line;
            String line2[];
            id = 0;
            while ((line = in.readLine())!= null){
                line2 = line.split(";");
                int c = Integer.parseInt(line2[6]);
                int idi = Integer.parseInt(line2[5]);
                if (c == 1){
                    ArrayList<String> list3 = new ArrayList<>();
                    list3.add(line2[7]);
                    list3.add(line2[8]);
                    list3.add(line2[9]);
                    list3.add(line2[10]);
                    double w = Double.parseDouble(line2[2]);
                    double dist = Double.parseDouble(line2[4]);
                    boolean b = Boolean.parseBoolean(line2[3]);
                    Package pack = new FirstClass(line2[0],line2[1],w,b);
                    pack.setArray(list3);
                    pack.setDistance(dist);
                    pack.setFrom(line2[11]);
                    pack.setTo(line2[12]);
                    pack.setId(idi);
                    size += 1;
                    id = idi;
                    //pack.setId(id);
                    list.add(pack);
                    
                }
                else if (c == 2) {
                    ArrayList<String> list3 = new ArrayList<>();
                    list3.add(line2[7]);
                    list3.add(line2[8]);
                    list3.add(line2[9]);
                    list3.add(line2[10]);
                    double w = Double.parseDouble(line2[2]);
                    double dist = Double.parseDouble(line2[4]);
                    boolean b = Boolean.parseBoolean(line2[3]);
                    Package pack = new SecondClass(line2[0],line2[1],w,b);
                    pack.setArray(list3);
                    pack.setDistance(dist);
                    pack.setFrom(line2[11]);
                    pack.setTo(line2[12]);
                    pack.setId(idi);
                    size += 1;
                    id = idi;
                    pack.setId(id);
                    list.add(pack);
                    
                } else {
                    ArrayList<String> list3 = new ArrayList<>();
                    list3.add(line2[7]);
                    list3.add(line2[8]);
                    list3.add(line2[9]);
                    list3.add(line2[10]);
                    double w = Double.parseDouble(line2[2]);
                    double dist = Double.parseDouble(line2[4]);
                    boolean b = Boolean.parseBoolean(line2[3]);
                    Package pack = new ThirdClass(line2[0],line2[1],w,b);
                    pack.setArray(list3);
                    pack.setDistance(dist);
                    pack.setFrom(line2[11]);
                    pack.setTo(line2[12]);
                    pack.setId(idi);
                    size += 1;
                    id = idi;
                    pack.setId(id);
                    list.add(pack);
                }
            }
        }
    }
    
    /**
     * Gives start city of package
     * 
     * @param i
     * @return 
     */
    public String getFrom(int i){
        return list.get(i).getFrom();
    }
    
    /**
     * Gives target city of package
     * 
     * @param i
     * @return 
     */
    public String getTo(int i){
        return list.get(i).getTo();
    }
    
}
