package harkkatyö;

/**
 * Author: Teemu Tynkkynen
 * Student number: 0418815
 * Date: 16.12.2015
 * 
 * Creates latiude and longitude to smartpost object
 */
public class GeoPoint {
    private final String lat; //latiude of smartpost
    private final String lng; //longitude of smartpost
    
    public GeoPoint(String la, String ln){
        lat = la;
        lng = ln; 
    }
    
    /**
     * Gives latiude
     * 
     * @return 
     */
    public String getLat(){
        return lat;
    }
    
    /**
     * Gives longitude
     * 
     * @return 
     */
    public String getLng(){
        return lng;
    } 
}
