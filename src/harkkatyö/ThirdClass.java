package harkkatyö;

/**
 * Author: Teemu Tynkkynen
 * Student number: 0418815
 * Date: 17.12.2015
 *
 * Third class package
 */
public class ThirdClass extends Package{
    
    public ThirdClass(String a, String b, double c, boolean d ) {
        super(a,b,c,d,3);
    } 
}
